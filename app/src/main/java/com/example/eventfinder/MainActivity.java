package com.example.eventfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_KEYWORD = "com.example.eventfinder.KEYWORD";
    public static final String EXTRA_DATE = "com.example.eventfinder.DATE";
    public static final String EXTRA_CATEGORY = "com.example.eventfinder.CATEGORY";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        ImageView titleImage = (ImageView) findViewById(R.id.item_header_view);
        Picasso.get().load("https://www.eventfinda.co.nz/images/global/attribution.gif?pwiomi").into(titleImage);
    }

    /** Called when user taps send**/
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editKeyword = (EditText) findViewById(R.id.keywordInput);
        String keyword = editKeyword.getText().toString();
        EditText editDate = (EditText) findViewById(R.id.categoryInput);
        String date = editDate.getText().toString();
        Spinner categorySpinner = (Spinner) findViewById(R.id.spinner);
        String category = categorySpinner.getSelectedItem().toString();
        intent.putExtra(EXTRA_KEYWORD, keyword);
        intent.putExtra(EXTRA_DATE, date);
        intent.putExtra(EXTRA_CATEGORY, category);

        startActivity(intent);
    }
}
