package com.example.eventfinder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Adapters.EventAdapter;
import Listeners.EventFindaListener;
import Utilities.EventFindaConnection;

public class DisplayMessageActivity extends AppCompatActivity {

    JSONArray events = new JSONArray();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter eventAdapter;
    private RecyclerView.LayoutManager layoutManager;


    public void setEvents(JSONArray events) {
        this.events = events;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String keyword = intent.getStringExtra(MainActivity.EXTRA_KEYWORD);
        String date = intent.getStringExtra(MainActivity.EXTRA_DATE);
        String formattedDate = "";
        String[] dates = date.split("/");
        if (dates.length == 3) {
            formattedDate = dates[2] + "-" + dates[1] + "-" + dates[0];
            System.out.println(formattedDate);
        }
        String category = intent.getStringExtra(MainActivity.EXTRA_CATEGORY);

        // Capture the layout's TextView and set the string as its text
        TextView keywordView = findViewById(R.id.keywordView);
        keywordView.setText(keyword);

        TextView dateView = findViewById(R.id.dateView);
        dateView.setText(date);

        TextView categoryView = findViewById(R.id.categoryView);
        categoryView.setText(category);

        EventFindaListener eventFindaListener = new EventFindaListener() {
            @Override
            public void parseEvents(JSONArray events) {
                try {
                    // data to populate the RecyclerView with
                    ArrayList<String> eventNames = new ArrayList<>();
                    ArrayList<String> imageUrls = new ArrayList<>();
                    ArrayList<String> dates = new ArrayList<>();
                    for (int i = 0 ; i < events.length(); i++) {
                        JSONObject obj = events.getJSONObject(i);
                        eventNames.add(obj.getString("name"));
                        dates.add(obj.getString("datetime_start"));
                        System.out.println(obj.getJSONObject("images").getJSONArray("images").getJSONObject(0).getJSONObject("transforms").getJSONArray("transforms").getJSONObject(0).getString("url"));
                        imageUrls.add(obj.getJSONObject("images").getJSONArray("images").getJSONObject(0).getJSONObject("transforms").getJSONArray("transforms").getJSONObject(2).getString("url"));
                    }
                    setUpRecyclerView(eventNames, imageUrls, dates);
                    System.out.println(eventNames.size());
                } catch (Exception e) {

                }

            }
        };
        EventFindaConnection.getEvents(keyword, category, formattedDate, 0, eventFindaListener);









    }

    private void setUpRecyclerView(ArrayList<String> eventNames, ArrayList<String> imageUrls, ArrayList<String> dates) {
        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        eventAdapter = new EventAdapter(this, eventNames, imageUrls, dates);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(eventAdapter);
    }
}
