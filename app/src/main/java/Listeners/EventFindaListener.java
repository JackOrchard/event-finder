package Listeners;

import org.json.JSONArray;

public interface EventFindaListener {
    void parseEvents(JSONArray events);
}
