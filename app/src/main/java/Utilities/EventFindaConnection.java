package Utilities;

import android.os.Debug;
import android.util.Log;

import com.example.eventfinder.DisplayMessageActivity;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import Listeners.EventFindaListener;
import cz.msebera.android.httpclient.Header;


public class EventFindaConnection {


    public static void getEvents(String keyword, String category, String startDate, Integer offset, final EventFindaListener eventListener) {

        String url = "events.json?rows=20" + "&offset=" + offset;

        /*if (!category.isEmpty()) {
            url += "&category="+category;
        }*/
        if (!startDate.isEmpty()) {
            url += "&start_date="+startDate;
        }


        if (!keyword.isEmpty()) {
            url = addKeyWordFilterToQuery(url, keyword);
        }
        getEventfindaResponse(url, eventListener);
    }


    /**
     * Adds keyword parameter to the API given query.
     *
     * The "keywords" parameter should be a string of one or more keywords.
     * If they are separated by "," then an "OR" search is performed, and
     * if they are separated by spaces then an "AND" search is performed.
     * For example:
     * Input: addKeyWordFilterToQuery("currentUrl", "cycling    Southland    ,   running, alpha")
     * Output: currentUrl&q=(cycling+AND+Southland)+OR+(running)+OR+(alpha)
     *
     * @return returns the id of given place name.
     */
    public static String addKeyWordFilterToQuery(String currentQuery, String keywords){
        String updatedQuery = currentQuery;
        keywords = keywords.trim();
        try {
            keywords = URLDecoder.decode(keywords, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println(e);
        }
        String[] qParams = keywords.split("\\s*,\\s*");
        List<String[]> ors = new ArrayList<>();
        for (String words: qParams) {
            String[] splitWords = words.split("\\s+");
            ors.add(splitWords);
        }
        String stringToAdd = "";
        int index = 0;
        for (String[] str : ors) {
            if (str.length > 0) {
                String andString = "(";
                for (String word : str) {
                    andString = andString.concat(word + "+AND+");
                }
                andString = removeCharsFromEnd(andString, 5);
                andString = andString.concat(")");
                stringToAdd = stringToAdd.concat(andString);
            }
            stringToAdd = stringToAdd.concat("+OR+");
            index++;
        }
        stringToAdd = removeCharsFromEnd(stringToAdd, 4);
        updatedQuery = updatedQuery.concat("&q=" + stringToAdd);

        return updatedQuery;
    }

    private static void getEventfindaResponse(String targetUrl, final EventFindaListener eventListener) {

            try {
                String usernameEventFinda = "travelea";
                String passwordEventFinda = "kq4mf8czrp92";
                String userCredentials = usernameEventFinda + ":" + passwordEventFinda;
                String credentials = "Basic " + new String(Base64.getEncoder().encodeToString(userCredentials.getBytes()));
                RequestParams rp = new RequestParams();
                System.out.println(targetUrl);
                JsonHttpResponseHandler responseHandler =new JsonHttpResponseHandler() {
                    JSONArray eventsArray = null;
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
                        try {
                            JSONObject serverResp = new JSONObject(response.toString());
                            eventsArray = serverResp.getJSONArray("events");
                            eventListener.parseEvents(eventsArray);

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        System.out.println(responseString);
                        super.onFailure(statusCode, headers, responseString, throwable);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        System.out.println(response);
                    }


                };
                HTTPUtils.get(targetUrl, credentials, rp, responseHandler);
                System.out.println(responseHandler);
            } catch (Exception e){
                System.out.println(e);
            }
    }

    private static String removeCharsFromEnd(String str, int numOfChars) {
        return str.substring(0, str.length() - numOfChars);
    }



}
