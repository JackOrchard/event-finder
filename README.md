# Event Finder App for Android

This is a personally created application.

<p align="center">
  <a href="https://imgur.com/ZUyIFxF"><img src="https://i.imgur.com/ZUyIFxF.jpg" title="source: imgur.com" width=200 height=400/></a>
  <a href="https://imgur.com/g3m763u"><img src="https://i.imgur.com/g3m763u.jpg" title="source: imgur.com" width=200 height=400/></a>
</p>

## About Event Finder

Event Finder is an event searcher that uses the Eventfinda API to search for events based on various parameters. The idea is to use it while travelling and looking for something to do.

This was originally designed as a personal project to extend the University Project  from SENG302:

[https://gitlab.com/JackOrchard/team-800](https://gitlab.com/JackOrchard/team-800)


## Getting Started

### Clone the Repository

Get started by cloning the project to your local machine:

```
https://gitlab.com/JackOrchard/event-finder.git
```


### Open and Run Project in Android Studio

Now that you have cloned the repo:

1. Open the project in Android Studio
2. Run with an emulator or your own device
3. Internet enable the device it is running on

### Licences 
The application uses the Eventfinda API.